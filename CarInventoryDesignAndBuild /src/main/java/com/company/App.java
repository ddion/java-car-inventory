package com.company;

import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.util.*;
import java.io.*;

public class App {


    public void addCar() {
        Scanner scan = new Scanner(System.in);
        Car car = new Car();
        try {
            System.out.println("Please enter make.");
            car.setMake(scan.nextLine().toLowerCase());

            System.out.println("Please enter the model.");
            car.setModel(scan.nextLine().toLowerCase());

            System.out.println("Please enter year.");
            car.setYear(Integer.parseInt(scan.nextLine()));

            System.out.println("Please enter the color.");
            car.setColor(scan.nextLine().toLowerCase());

            System.out.println("Please enter mileage.");
            car.setMiles(Integer.parseInt(scan.nextLine()));

            Car.getCarList().add(car);
            saveList();
        } catch (NumberFormatException e) {
            System.err.println("Invalid input. Please enter a number next time.");
        }
    }

    public void switchSearch() {
        Scanner scan = new Scanner(System.in);
        boolean flag2 = true;
        do {
            try {
                System.out.println("\nHow would you like to search by\n" +
                        "\t1.Make | 2.Model | 3.Year | 4.Color | 5.Mileage | 6.Exit Back to Main Menu |");
                int userInput = Integer.parseInt(scan.nextLine());

                switch(userInput) {
                    case 1:
                        makeSearch();
                        break;
                    case 2:
                        modelSearch();
                        break;
                    case 3:
                        yearSearch();
                        break;
                    case 4:
                        colorSearch();
                        break;
                    case 5:
                        milesSearch();
                        break;
                    case 6:
                        flag2 = false;
                        break;
                    default:
                        System.err.println("You have not chosen a valid option from the list.");
                }
            } catch (NumberFormatException e) {
                System.err.println("Invalid input. Please enter a valid number from the following options. ↓↓↓");
            }
        } while (flag2);

    }

    public void makeSearch() {
        Scanner scan = new Scanner(System.in);
        System.out.println("What make are you looking for?");
        String make = scan.nextLine().toLowerCase();
        boolean ifExists = Car.getCarList().stream().anyMatch(c->c.getMake().equals(make));
        if (!ifExists) {
            System.out.println("\n=================================================");
            System.out.println("There are no cars that match that description.");
            System.out.println("=================================================");
        }
        else {
            Car.getCarList().stream()
                    .filter(c -> c.getMake().equals(make))
                    .forEach(car -> {
                        System.out.println("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~");
                        System.out.println("Make: " + car.getMake() + " | Model: " + car.getModel() + " | Year: " + car.getYear() + " | Color: " + car.getColor() + " | Mileage: " + car.getMiles());
                    });
        }
    }

    public void modelSearch() {
        Scanner scan = new Scanner(System.in);
        System.out.println("What model are you looking for?");
        String model = scan.nextLine().toLowerCase();
        boolean ifExists = Car.getCarList().stream().anyMatch(c->c.getModel().equals(model));
        if (!ifExists) {
            System.out.println("\n=================================================");
            System.out.println("There are no cars that match that description.");
            System.out.println("=================================================");
        }
        else {
            Car.getCarList().stream()
                    .filter(c->c.getModel().equals(model))
                    .forEach(car-> {
                        System.out.println("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~");
                        System.out.println("Make: " + car.getMake() + " | Model: " + car.getModel() + " | Year: " + car.getYear() + " | Color: " + car.getColor() + " | Mileage: " + car.getMiles() );
                    });
        }
    }

    public void yearSearch() {
        Scanner scan = new Scanner(System.in);
        System.out.println("What year are you looking for?");
        int year = Integer.parseInt(scan.nextLine());
        boolean ifExists = Car.getCarList().stream().anyMatch(c->c.getYear() == year);
        if (!ifExists) {
            System.out.println("\n=================================================");
            System.out.println("There are no cars that match that description.");
            System.out.println("=================================================");
        }
        else {
            Car.getCarList().stream()
                    .filter(c->c.getYear() == year)
                    .forEach(car-> {
                        System.out.println("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~");
                        System.out.println("Make: " + car.getMake() + " | Model: " + car.getModel() + " | Year: " + car.getYear() + " | Color: " + car.getColor() + " | Mileage: " + car.getMiles() );
                    });
        }
    }

    public void colorSearch() {
        Scanner scan = new Scanner(System.in);
        System.out.println("What color are you looking for?");
        String color = scan.nextLine().toLowerCase();
        boolean ifExists = Car.getCarList().stream().anyMatch(c->c.getColor().equals(color));
        if (!ifExists) {
            System.out.println("\n=================================================");
            System.out.println("There are no cars that match that description.");
            System.out.println("=================================================");
        }
        else {
            Car.getCarList().stream()
                    .filter(c->c.getColor().equals(color))
                    .forEach(car-> {
                        System.out.println("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~");
                        System.out.println("Make: " + car.getMake() + " | Model: " + car.getModel() + " | Year: " + car.getYear() + " | Color: " + car.getColor() + " | Mileage: " + car.getMiles() );
                    });
        }
    }

    public void milesSearch() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Under what mileage are you looking for?");
        int miles = Integer.parseInt(scan.nextLine());
        boolean ifExists = Car.getCarList().stream().anyMatch(c->c.getMiles() <= miles);
        if (!ifExists) {
            System.out.println("\n=================================================");
            System.out.println("There are no cars that match that description.");
            System.out.println("=================================================");
        }
        else {
            Car.getCarList().stream()
                    .filter(c->c.getMiles() <= miles)
                    .forEach(car-> {
                        System.out.println("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~");
                        System.out.println("Make: " + car.getMake() + " | Model: " + car.getModel() + " | Year: " + car.getYear() + " | Color: " + car.getColor() + " | Mileage: " + car.getMiles() );
                    });
        }
    }

    public void listAll() {
        Car.getCarList().stream()
                .forEach(car-> {
                    System.out.println("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~");
                    System.out.println("Make: " + car.getMake() + " | Model: " + car.getModel() + " | Year: " + car.getYear() + " | Color: " + car.getColor() + " | Mileage: " + car.getMiles() );
                });
    }


    public void delete() {
        Scanner scan = new Scanner(System.in);
        System.out.println("From the list which car would you like to delete?");
        for (int i = 0; i <Car.getCarList().size() ; i++) {
            System.out.println("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~");
            System.out.println("ID#: " + i + " | Make: " + Car.getCarList().get(i).getMake() + " | Model: " + Car.getCarList().get(i).getModel() + " | Year: " + Car.getCarList().get(i).getYear() + " | Color: " + Car.getCarList().get(i).getColor() + " | Mileage: " + Car.getCarList().get(i).getMiles() );
        }
        System.out.println("Enter the ID number of the car you wish to remove.");
        int userInput = Integer.parseInt(scan.nextLine());
        Car.getCarList().remove(userInput);
        System.out.println("The car with ID# "+ userInput + " has been removed.");
        saveList();
    }

    public static void seedCarList() {
        try {
            Car.setCarList(new CsvToBeanBuilder<Car>(new FileReader("carListCsv.csv")).withType(Car.class).build().parse());
            } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }

    }

    public static void saveList() {
        try {
            for(Car car : Car.getCarList()) {
                Writer writer = new FileWriter("carListCsv.csv");

                StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
                beanToCsv.write(Car.getCarList());
                writer.close();
            }
        } catch (FileNotFoundException e) {
            System.out.println("Could not find CSV file: " + e.getMessage());
        } catch (IOException | CsvDataTypeMismatchException | CsvRequiredFieldEmptyException e) {
            System.out.println("ERROR: Something went wrong writing your CSV file: " + e.getMessage());
        }
    }


    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        boolean flag = true;
        App app = new App();

//        List<Car> carList = new ArrayList<>();
//        Car car = new Car();
//        Car testCar = new Car("Chevy","Camaro", 1976, "Blue", 40000);
//        carList.add(testCar);

        System.out.println("Welcome to JavaCars");
         seedCarList();


        do {
            try {
                System.out.println("\nPlease select from the following options \n" +
                        "\t1.Add Car | 2.Delete Car | 3.List All Available Cars | 4.Search Cars | 5.Exit Program |");
                int userInput = Integer.parseInt(scan.nextLine());
                switch(userInput) {
                    case 1:
                        app.addCar();
                        break;
                    case 2:
                        app.delete();
                        break;
                    case 3:
                        app.listAll();
                        break;
                    case 4:
                        app.switchSearch();
                        break;
                    case 5:
                        flag = false;
                        break;
                    default:
                        System.err.println("You have not chosen a valid option from the list.");
                }
            } catch (NumberFormatException e) {
                System.err.println("Invalid input. Please enter a valid number from the following options. ↓↓↓");
            }

        } while (flag);


    }

}

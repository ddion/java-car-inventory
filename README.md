# Java_Car_Inventory

- A simple command line java application with persisting data that keeps track of an inventory of cars. 

* This application tracks the following data:
    * Make
    * Model
    * Year
    * Color
    * Mileage
    <br />
* You may:
    * Add a car
    * Delete a car
    * List all cars
    * Search for car

![Java Cars](images/javaCars.png)
